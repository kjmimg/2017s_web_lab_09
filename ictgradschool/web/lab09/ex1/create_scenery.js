/**
 * Created by jkua423 on 7/12/2017.
 */


$(function(){
    $("#radio-background1").click(function(){
       $("#background").attr("src","../images/background1.jpg");
    });

    $("#radio-background2").click(function() {
        $("#background").attr("src", "../images/background2.jpg");
    });

    $("#radio-background3").click(function(){
        $("#background").attr("src","../images/background3.jpg");
    });

    $("#radio-background4").click(function() {
        $("#background").attr("src", "../images/background4.jpg");
    });

    $("#radio-background5").click(function(){
        $("#background").attr("src","../images/background5.jpg");
    });

    $("#radio-background6").click(function() {
        $("#background").attr("src", "../images/background6.gif");
    });

    $("#check-dolphin1").click(function(){
       if($("#check-dolphin1").is(':checked'))
           $("#dolphin1").show();
       else
           $("#dolphin1").hide();
    });

    $("#check-dolphin2").click(function(){
        if($("#check-dolphin2").is(':checked'))
            $("#dolphin2").show();
        else
            $("#dolphin2").hide();
    });

    $("#check-dolphin3").click(function(){
        if($("#check-dolphin3").is(':checked'))
            $("#dolphin3").show();
        else
            $("#dolphin3").hide();
    });

    $("#check-dolphin4").click(function(){
        if($("#check-dolphin4").is(':checked'))
            $("#dolphin4").show();
        else
            $("#dolphin4").hide();
    });

    $("#check-dolphin5").click(function(){
        if($("#check-dolphin5").is(':checked'))
            $("#dolphin5").show();
        else
            $("#dolphin5").hide();
    });

    $("#check-dolphin6").click(function(){
        if($("#check-dolphin6").is(':checked'))
            $("#dolphin6").show();
        else
            $("#dolphin6").hide();
    });

    $("#check-dolphin7").click(function(){
        if($("#check-dolphin7").is(':checked'))
            $("#dolphin7").show();
        else
            $("#dolphin7").hide();
    });

    $("#check-dolphin8").click(function(){
        if($("#check-dolphin8").is(':checked'))
            $("#dolphin8").show();
        else
            $("#dolphin8").hide();
    });

    $("#size-control").slider({

        slide: function(event, ui){
            $("#background").css("width",ui.value+"%");
        }
    });


});